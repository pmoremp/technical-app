<div class="form-group">
    <label for="Name">First name*:</label>
    <input type="text" class="form-control" name="first_name" id="editFirstName" value="{{$employee->first_name}}">
</div>
<div class="form-group">
    <label for="Name">Last name*:</label>
    <input type="text" class="form-control" name="last_name" id="editLastName" value="{{$employee->last_name}}">
</div>
<div class="form-group">
    <label for="email">Email*:</label>
    <input type="text" class="form-control" name="email" id="editEmail" value="{{$employee->email_address}}">
</div>

<div class="form-group">
    <label for="Company">Company*:</label>
    <select class="form-control" name="company_id" id="editCompanyId">
        @foreach($companies as $company)
            <option value="{{$company->id}}" {{ ($employee->company_id ===  $company->id) ? 'selected' : ''}} >{{$company->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="email">Position*:</label>
    <input type="text" class="form-control" name="position" id="editPosition" value="{{$employee->position}}">
</div>
<div class="form-group">
    <label for="email">City*:</label>
    <input type="text" class="form-control" name="city" id="editCity" value="{{$employee->city}}">
</div>
<div class="form-group">
    <label for="email">Country*:</label>
    <input type="text" class="form-control" name="country" id="editCountry" value="{{$employee->country}}">
</div>
<div class="form-group">
    <label for="Company">Status*:</label>
    <select class="form-control" name="status" id="editStatus">
        <option value="1" {{ $employee->status ? 'selected' : ''}}>Active</option>
        <option value="0" {{ !$employee->status ? 'selected' : ''}}>Inactive</option>
    </select>
</div>
