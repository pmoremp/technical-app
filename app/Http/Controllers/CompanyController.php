<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\SaveRequest;
use App\Models\Company;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\Facades\DataTables;

class CompanyController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('companies.index');
    }

    public function listing(){
        return DataTables::of(Company::query())
            ->addColumn('Actions', function($data) {
                return '<button type="button" class="btn btn-success btn-sm" id="getEditCompanyData" data-id="'.$data->id.'">Edit</button>
                    <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteCompanyModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }

    /**
     * @param SaveRequest $request
     * @return JsonResponse
     */
    public function store(SaveRequest $request): JsonResponse
    {
        $company = Company::create($request->all());
        return Response()->json(['data' => $company, 'success' => true]);
    }

    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function edit(Company $company): JsonResponse
    {
        $html = view('companies.edit', compact('company'))->render();
        return response()->json(['html' => $html, 'success' => true]);
    }

    /**
     * @param SaveRequest $request
     * @param Company $company
     * @return JsonResponse
     */
    public function update(SaveRequest $request, Company $company): JsonResponse
    {
        $company->update($request->all());
        return Response()->json(['data' => $company->fresh(), 'success' => true]);
    }

    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function destroy(Company $company): JsonResponse
    {
        $company->delete();
        return Response()->json(['success' => true]);
    }
}
