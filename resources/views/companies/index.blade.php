@extends('layouts.app')



@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Companies
                        <button style="float: right; font-weight: 900;" class="btn btn-info btn-sm" type="button"
                                data-toggle="modal" data-target="#CreateCompanyModal">
                            Create Company
                        </button>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered datatable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Website</th>
                                    <th>Description</th>
                                    <th width="150" class="text-center">Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="CreateCompanyModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Company Create</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Company was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <label for="Name">Name*:</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="Name">Type*:</label>
                        <select class="form-control" name="type" id="type">
                            <option value="Public Limited Company">Public Limited Company</option>
                            <option value="Private Limited Company">Private Limited Company</option>
                            <option value="Registered Company">Registered Company</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Name">Website*:</label>
                        <input type="url" class="form-control" name="website" id="website">
                    </div>
                    <div class="form-group">
                        <label for="Name">Description:</label>
                        <textarea class="form-control" name="description" id="description"></textarea>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="SubmitCompanyForm">Create</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Company Modal -->
    <div class="modal" id="EditCompanyModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit</h4>
                    <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="alert edit-alert-danger alert-dismissible fade show" role="alert"
                         style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Updated.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="EditCompanyModalBody">

                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="SubmitEditCompanyForm">Update</button>
                    <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Company Modal -->
    <div class="modal" id="DeleteCompanyModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Delete Company</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <p>Are you sure want to delete this company?</p>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="SubmitDeleteCompanyForm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end bootstrap model -->

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            // init datatable.
            let dataTable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                pageLength: 25,
                // scrollX: true,
                "order": [[0, "desc"]],
                ajax: '{{ route('get-companies') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'type', name: 'type'},
                    {data: 'website', name: 'website'},
                    {data: 'description', name: 'description'},
                    {data: 'Actions', name: 'Actions', orderable: false, serachable: false, sClass: 'text-center'},
                ]
            });

            // Create Company Ajax request.
            $('#SubmitCompanyForm').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('companies.store') }}",
                    method: 'post',
                    data: {
                        name: $('#name').val(),
                        type: $('#type').val(),
                        website: $('#website').val(),
                        description: $('#description').val(),
                    },
                    success: function (result) {
                        if (result.success) {
                            $('.alert-danger').hide();
                            $('.datatable').DataTable().ajax.reload();
                            $('#name').val('');
                            $('#type').val('');
                            $('#website').val('');
                            $('#description').val('');
                            $('#CreateCompanyModal').modal('hide');
                        }
                    },
                    error: function (error) {
                        if (error.responseJSON && error.responseJSON.errors) {
                            $('.alert-danger').html('');
                            $.each(error.responseJSON.errors, function (key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<strong><li>' + value + '</li></strong>');
                            });
                        }
                    }
                });
            });

            // Get single Company in EditModel
            $('.modelClose').on('click', function () {
                $('#EditCompanyModal').hide();
            });
            var id;
            $('body').on('click', '#getEditCompanyData', function (e) {
                e.preventDefault();
                $('.alert-danger').html('');
                $('.alert-danger').hide();
                id = $(this).data('id');
                $.ajax({
                    url: "companies/" + id + "/edit",
                    method: 'GET',
                    success: function (result) {
                        $('#EditCompanyModalBody').html(result.html);
                        $('#EditCompanyModal').show();
                    }
                });
            });

            // Update company Ajax request.
            $('#SubmitEditCompanyForm').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "companies/" + id,
                    method: 'PUT',
                    data: {
                        name: $('#editName').val(),
                        type: $('#editType').val(),
                        website: $('#editWebsite').val(),
                        description: $('#editDescription').val()
                    },
                    success: function (result) {
                        if (result.success) {
                            $('.edit-alert-danger').hide();
                            $('.datatable').DataTable().ajax.reload();
                            $('#EditCompanyModal').hide();
                        }
                    },
                    error: function (error) {
                        if (error.responseJSON && error.responseJSON.errors) {
                            $('.edit-alert-danger').html('');
                            $.each(error.responseJSON.errors, function (key, value) {
                                $('.edit-alert-danger').show();
                                $('.edit-alert-danger').append('<strong><li>' + value + '</li></strong>');
                            });
                        }
                    }
                });
            });

            // Delete company Ajax request.
            let deleteID;
            $('body').on('click', '#getDeleteId', function () {
                deleteID = $(this).data('id');
            })
            $('#SubmitDeleteCompanyForm').click(function (e) {
                e.preventDefault();
                let id = deleteID;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "companies/" + id,
                    method: 'DELETE',
                    success: function (result) {
                        if (result.success) {
                            $('#DeleteCompanyModal').modal('hide');
                            $('.datatable').DataTable().ajax.reload();
                        }
                    }
                });
            });
        });
    </script>
@endsection

