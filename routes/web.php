<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

// companies routes
Route::get('companies/listing', [CompanyController::class, 'listing'])->name('get-companies');
Route::resource('companies', CompanyController::class);


// employee routes
Route::get('employees/listing', [EmployeeController::class, 'listing'])->name('get-employees');
Route::resource('employees', EmployeeController::class);


Route::model('company', Company::class);
Route::model('employee', Employee::class);


