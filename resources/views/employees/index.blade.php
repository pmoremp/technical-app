@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Company :
                        <select id="companyDropdownId">
                            <option value="">Select company</option>
                            @foreach($companies as $company)
                                <option value="{{$company->id}}">{{$company->name}}</option>
                            @endforeach
                        </select>


                        <button style="float: right; font-weight: 900;" class="btn btn-info btn-sm" type="button"
                                data-toggle="modal" data-target="#CreateEmployeeModal">
                            Create Employee
                        </button>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered datatable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Position</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                    <th width="150" class="text-center">Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="modal" id="CreateEmployeeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Employee Create</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                            <strong>Success!</strong>Employee was added successfully.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="form-group">
                            <label for="Name">First name*:</label>
                            <input type="text" class="form-control" name="first_name" id="firstName">
                        </div>
                        <div class="form-group">
                            <label for="Name">Last name*:</label>
                            <input type="text" class="form-control" name="last_name" id="lastName">
                        </div>
                        <div class="form-group">
                            <label for="email">Email*:</label>
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="Company">Company*:</label>
                            <select class="form-control" name="company_id" id="companyId">
                                @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Position*:</label>
                            <input type="text" class="form-control" name="position" id="position">
                        </div>
                        <div class="form-group">
                            <label for="email">City*:</label>
                            <input type="text" class="form-control" name="city" id="city">
                        </div>
                        <div class="form-group">
                            <label for="email">Country*:</label>
                            <input type="text" class="form-control" name="country" id="country">
                        </div>
                        <div class="form-group">
                            <label for="Company">Status*:</label>
                            <select class="form-control" name="status" id="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="SubmitEmployeeForm">Create</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Edit employee Modal -->
        <div class="modal" id="EditEmployeeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Edit</h4>
                        <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="alert edit-alert-danger alert-dismissible fade show" role="alert"
                             style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                            <strong>Success!</strong>Updated.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div id="EditEmployeeModalBody">

                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="SubmitEditEmployeeForm">Update</button>
                        <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Delete employee Modal -->
        <div class="modal" id="DeleteEmployeeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Employee</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <p>Are you sure want to delete this employee?</p>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="SubmitDeleteEmployeeForm">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end bootstrap model -->

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            // init datatable.
            let dataTable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                pageLength: 25,
                // scrollX: true,
                "order": [[0, "desc"]],
                ajax: '{{ route('get-employees') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email_address', name: 'email_address'},
                    {data: 'company.name', name: 'company_id', orderable: false, serachable: false},
                    {data: 'position', name: 'position'},
                    {data: 'city', name: 'city'},
                    {data: 'country', name: 'country'},
                    {
                        data: 'status', render: function (data) {
                            return data ? 'Active' : 'Inactive';
                        }
                    },
                    {data: 'Actions', name: 'Actions', orderable: false, serachable: false, sClass: 'text-center'},
                ]
            });


            $('#companyDropdownId').on('change', function () {
                $('.datatable').DataTable().columns(4).search( this.value ).draw();
            } );

            // Create employee Ajax request.
            $('#SubmitEmployeeForm').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('employees.store') }}",
                    method: 'post',
                    data: {
                        first_name: $('#firstName').val(),
                        last_name: $('#lastName').val(),
                        email_address: $('#email').val(),
                        position: $('#position').val(),
                        city: $('#city').val(),
                        country: $('#country').val(),
                        status: $('#status').val(),
                        company_id: $('#companyId').val()
                    },
                    success: function (result) {
                        if (result.success) {
                            $('.alert-danger').hide();
                            $('.datatable').DataTable().ajax.reload();
                            $('#firstName').val('');
                            $('#lastName').val('');
                            $('#email').val('');
                            $('#position').val('');
                            $('#city').val('');
                            $('#country').val('');
                            $('#status').val('');
                            $('#CreateEmployeeModal').modal('hide');
                        }
                    },
                    error: function (error) {
                        if (error.responseJSON && error.responseJSON.errors) {
                            $('.alert-danger').html('');
                            $.each(error.responseJSON.errors, function (key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<strong><li>' + value + '</li></strong>');
                            });
                        }
                    }
                });
            });

            // Get single employee in EditModel
            $('.modelClose').on('click', function () {
                $('#EditEmployeeModal').hide();
            });
            let id;
            $('body').on('click', '#getEditEmployeeData', function (e) {
                e.preventDefault();
                $('.alert-danger').html('');
                $('.alert-danger').hide();
                id = $(this).data('id');
                $.ajax({
                    url: "employees/" + id + "/edit",
                    method: 'GET',
                    success: function (result) {
                        $('#EditEmployeeModalBody').html(result.html);
                        $('#EditEmployeeModal').show();
                    }
                });
            });

            // Update employee Ajax request.
            $('#SubmitEditEmployeeForm').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "employees/" + id,
                    method: 'PUT',
                    data: {
                        first_name: $('#editFirstName').val(),
                        last_name: $('#editLastName').val(),
                        email_address: $('#editEmail').val(),
                        position: $('#editPosition').val(),
                        city: $('#editCity').val(),
                        country: $('#editCountry').val(),
                        status: $('#editStatus').val(),
                        company_id: $('#editCompanyId').val()
                    },
                    success: function (result) {
                        if (result.success) {
                            $('.edit-alert-danger').hide();
                            $('.datatable').DataTable().ajax.reload();
                            $('#EditEmployeeModal').hide();
                        }
                    },
                    error: function (error) {
                        if (error.responseJSON && error.responseJSON.errors) {
                            $('.edit-alert-danger').html('');
                            $.each(error.responseJSON.errors, function (key, value) {
                                $('.edit-alert-danger').show();
                                $('.edit-alert-danger').append('<strong><li>' + value + '</li></strong>');
                            });
                        }
                    }
                });
            });

            // Delete employee Ajax request.
            let deleteID;
            $('body').on('click', '#getDeleteId', function () {
                deleteID = $(this).data('id');
            })
            $('#SubmitDeleteEmployeeForm').click(function (e) {
                e.preventDefault();
                let id = deleteID;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "employees/" + id,
                    method: 'DELETE',
                    success: function (result) {
                        if (result.success) {
                            $('#DeleteEmployeeModal').modal('hide');
                            $('.datatable').DataTable().ajax.reload();
                        }
                    }
                });
            });

        });
    </script>
@endsection

