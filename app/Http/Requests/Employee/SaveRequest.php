<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validation =  [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'position' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'status' => 'required|boolean',
            'company_id' => 'required|exists:companies,id'
        ];
        if (is_null(request()->employee)) {
            $validation['email_address'] = 'required|email|unique:employees,email_address,NULL,id,deleted_at,NULL';
        } else {
            $validation['email_address'] = 'required|email|unique:employees,email_address,'.request()->employee->id.',id,deleted_at,NULL';
        }
        return $validation;
    }
}
