<div class="form-group">
    <label for="Name">Name*:</label>
    <input type="text" class="form-control" name="name" id="editName" value="{{$company->name}}">
</div>
<div class="form-group">
    <label for="Name">Type*:</label>
    <select class="form-control" name="type" id="editType">
        <option value="Public Limited Company" {{ $company->type == 'Public Limited Company' ? 'selected' : '' }}>Public Limited Company</option>
        <option value="Private Limited Company" {{ $company->type == 'Private Limited Company' ? 'selected' : '' }}>Private Limited Company</option>
        <option value="Registered Company" {{ $company->type == 'Registered Company' ? 'selected' : '' }}>Registered Company</option>
    </select>
</div>

<div class="form-group">
    <label for="Name">Website*:</label>
    <input type="url" class="form-control" name="website" id="editWebsite" value="{{$company->website}}">
</div>

<div class="form-group">
    <label for="Name">Description:</label>
    <textarea class="form-control" name="description" id="editDescription">{{$company->description}}</textarea>
</div>
