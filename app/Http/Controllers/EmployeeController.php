<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\SaveRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $companies = Company::select(['id', 'name'])->get();
        return view('employees.index', compact('companies'));
    }

    public function listing(){
        return DataTables::of(Employee::with('company:id,name'))
            ->addColumn('Actions', function($data) {
                return '<button type="button" class="btn btn-success btn-sm" id="getEditEmployeeData" data-id="'.$data->id.'">Edit</button>
                    <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteEmployeeModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }

    /**
     * @param SaveRequest $request
     * @return JsonResponse
     */
    public function store(SaveRequest $request): JsonResponse
    {
        $employee = Employee::create($request->all());
        return Response()->json(['data' => $employee, 'success' => true]);
    }

    /**
     * @param Employee $employee
     * @return JsonResponse
     */
    public function edit(Employee $employee): JsonResponse
    {
        $companies = Company::select(['id', 'name'])->get();
        $html = view('employees.edit', compact('employee', 'companies'))->render();
        return response()->json(['html' => $html, 'success' => true]);
    }

    /**
     * @param SaveRequest $request
     * @param Employee $employee
     * @return JsonResponse
     */
    public function update(SaveRequest $request, Employee $employee): JsonResponse
    {
        $employee->update($request->all());
        return Response()->json(['data' => $employee->fresh(), 'success' => true]);
    }

    /**
     * @param Employee $employee
     * @return JsonResponse
     */
    public function destroy(Employee $employee): JsonResponse
    {
        $employee->delete();
        return Response()->json(['success' => true]);
    }
}
